const express = require("express")

const port = 4000

const application = express()

application.listen(port, () => console.log(`Express server is running on port ${port}`))

application.get('/', (request, response) => {
    //we will describe how the server would interact/respond back to the client.
    //we would need to transmit a message back to the client.
    //send() -> allows us transit or transmit data over a network.
    response.send('Greetings Welcome to course booking of Earl Diaz')
});